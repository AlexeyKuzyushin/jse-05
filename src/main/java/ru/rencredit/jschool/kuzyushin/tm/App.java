package ru.rencredit.jschool.kuzyushin.tm;

import ru.rencredit.jschool.kuzyushin.tm.constant.ITerminalConst;

public class App {

    public static void main(String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        parseArgs(args);
    }

    private static void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ITerminalConst.CMD_VERSION:
                showVersion();
                break;
            case ITerminalConst.CMD_ABOUT:
                showAbout();
                break;
            case ITerminalConst.CMD_HELP:
                showHelp();
                break;
        }
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Alexey Kuzyushin");
        System.out.println("E-MAIL: alexeykuzyushin@yandex.ru");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(ITerminalConst.CMD_ABOUT + " - Show developer info.");
        System.out.println(ITerminalConst.CMD_VERSION + " - Show version info.");
        System.out.println(ITerminalConst.CMD_HELP + " - Display terminal commands.");
    }
}
